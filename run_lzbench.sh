#/bin/bash

infile=$1
outdir=$2
cofile=${3:-compressors.txt}

. utils.sh

usage() {
    echo -e "\nUsage: $0 infile outdir [compressors.txt]\n"
    exit 1
}

main() {
    while IFS= read -r line; do
        [[ "${line}" =~ ^#.*$ ]] && continue
        [[ -z ${line} ]] && continue
        echo "${line}"
        name=$(echo ${line} | cut -d',' -f 1)
        outfile="${outdir}/out-${name}.csv"
        [[ -s ${outfile} ]] && continue
        lzbench -o4 -e${line} ${infile} > ${outfile} 2>&1
    done < "${cofile}"
}



if [  $# -lt 2 ]; then
    usage
elif [ $# -ge 2 ]; then
    file_exists ${infile}
    file_exists ${cofile}
    create_dir ${outdir}
    main ${infile} ${outdir} ${cofile}
fi

exit 0
