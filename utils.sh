#/bin/bash

create_dir() {
    local outdir=$1
    if [ -d "${outdir}" ]; then
        echo "Output directory ${outdir} alreadys exists."
    else
        echo "Creating directory: ${outdir}"
        mkdir -p ${outdir}
    fi
}

file_exists() {
    local infile=$1
    if [ ! -f "${infile}" ]; then
        echo "${infile} file does not exist"
        exit 1
    fi
}



