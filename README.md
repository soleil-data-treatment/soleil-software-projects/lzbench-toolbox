# Lzbench Toolbox

Toolbox to run lzbench

# Usage
```
./run_lzbench.sh infile outdir [compressors.txt]
```
The above command runs lzbench on the input file `infile` for all compressors and levels described in the `compressors.txt` file. \
The results are written in the directory `outdir`in separate files, each corresponding to a given compressor.


